# COVID-19 Ohio County-Level Data

*NOTE: The previous contents of this directory have been moved to the 00-ARCHIVE directory.  The current contents include modifications that have been made off-line since the original contents were published and are not necessarily drop-in replacements for the original contents.*

## Overview

Since March 2020, Ohio Department of Health has published COVID-19 case data and vaccination data in tabular formats:

  - Case data: [https://coronavirus.ohio.gov/static/dashboards/COVIDDeathData_CountyOfResidence.csv](https://coronavirus.ohio.gov/static/COVIDSummaryData.csv)
  - Vaccination data: [https://coronavirus.ohio.gov/static/dashboards/vaccine_data.csv](https://coronavirus.ohio.gov/static/COVIDSummaryData.csv)

These datasets are updated daily at or around 2pm.  

CURA maintains an ArcGIS Online Feature Service corresponding to each of the above datasets:

  - Cases: [COVID19 in Ohio](https://ohiostate.maps.arcgis.com/home/item.html?id=56132fc10d04421f917bf3c466c0216a)
  - Vaccinations: [COVID19 vaccination in Ohio](https://ohiostate.maps.arcgis.com/home/item.html?id=be56e3216953427796f3e16e8966d42e)

Both layers are owned by the "osucura" user.

Each day at or around 3pm, a CURA representative runs two arcpy/Python scripts, one to update each layer.  The scripts themselves are fully automated but for a variety of reasons it is inconvenient to trigger the scripts automatically.  It can be done, but it never seemed worth the effort.

The scripts and support files are included in the `covid-ohio` and `vaccines-ohio` folders.  Some adaptation is likely required to get these to run on your system.

In both cases, the key script is updateWebLayers.py.  The command to run the script is:

"c:\Program Files\ArcGIS\Pro\bin\Python\Scripts\propy.bat" updateWebLayers.py

Or you can save yourself some keystrokes by running run_script.bat for each case.

## License

[MIT License](https://opensource.org/licenses/MIT)

Copyright 2020 The Ohio State University

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
