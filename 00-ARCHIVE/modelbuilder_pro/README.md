# COVID-19 Ohio County-Level Data - ModelBuilder Scripts for ArcGIS Pro

## Introduction

The models in the `covid_odh.tbx` ModelBuilder toolbox in conjunction with the support files herein will retrieve [COVID-19 case data](https://coronavirus.ohio.gov/static/COVIDSummaryData.csv) from Ohio Department of Health, summarize it in various ways, and publish an ArcGIS Online feature layer comprised of a set of feature services and tables.

## Instructions

As of 4/3/2020, the process to use the ModelBuilder models is as follows:
  1. Create a new project in ArcGIS Pro (make sure it creates `Default.gdb`)
  2. Move `covid_odh.tbx` and support files to the project folder.
  3. Validate and run the model `covid_odh.tbx\ingestODHSummary`
  3. Validate and run the model `covid_odh.tbx\createOhioCountiesLayer`
  3. Manually publish the map contents as a web layer.  One method is as follows:
    - Log into ArcGIS Online
    - From the Map tab, click Share > Web Layer > Publish Web Layer
    - Fill out the Item Details and click Publish.  Wait for publishing to complete.
  3. Publish again, overwriting the existing layer (to produce the correct service draft file)
    - From the Map tab, click Share > Web Layer > Overwrite Web Layer
    - Select the web layer you just created from your portal content.  Click OK  Wait for publishing to complete.
  3. Locate the service draft file for the last publishing operation.  You'll find it here:

  ```
  C:\Users\<username>\AppData\Local\ESRI\ArcGISPro\Staging\SharingProcesses\<processId>\<serviceName>.sddraft
  ```

  where `<username>` is the user you are logged in as, `<processId>` is the number corresponding to the last sharing operation (probably the largest number), and `<serviceName>` is some variant of the service name you defined in the Item Details.


  3. Copy the service draft to the project folder.
  3. Edit the model `covid_odh.tbx\publishWebLayers`.  Update the "Service Definition Draft" parameter of the "Stage Service" tool to point to the service draft file in the project directory.  Save the model.
  3. Validate and run the model `covid_odh.tbx\publishWebLayers`
  4. Log into ArcGIS Online and verify that the service has been published correctly.  

## Model Exports

To simplify configuration management and review, the latest versions of models referenced above have been exported as SVG images and Python scripts.  These are included in the `model_exports` folder.

## License

[MIT License](https://opensource.org/licenses/MIT)

Copyright 2020 The Ohio State University

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
