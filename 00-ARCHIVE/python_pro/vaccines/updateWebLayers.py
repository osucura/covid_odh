# updateWebLayersVaccinations.py
# Source: COVID-19 Ohio County-Level Data (https://gitlab.com/osucura/covid_odh)
# Author: Center for Urban and Regional Analysis
# License: MIT License
# Copyright 2021 The Ohio State University

import arcpy
import sys
import os.path
import os
import urllib.request
import shutil
from datetime import datetime

# Script configuration
LOGFILE = os.path.normpath("./updateWebLayersVaccinations.log")
DEFAULT_GDB = os.path.normpath("./Default.gdb")
targetFile = "VaccinationsSummaryData.csv"
targetFolder = os.path.normpath("./input_data")
sourceUrl = "https://coronavirus.ohio.gov/static/dashboards/vaccine_data.csv"
countyFeatures = os.path.normpath("./input_data/cb_2018_us_county_500k/cb_2018_us_county_500k.shp")
countyPopulations = os.path.normpath("./input_data/county_populations/county_populations.csv")
sumCountyPop = 11655397   # from county_populations.csv
vaccinationsLayerFile = os.path.normpath("./Vaccinations (graduated).lyrx")
boundariesLayerFile = os.path.normpath("./Counties.lyrx")
serviceDraft = os.path.normpath(r"C:\Users\porr.4\Desktop\covid-ohio\vaccines-ohio\COVID19_vaccination_in_Ohio.sddraft")
folderOverride = False
#folderType = "EXISTING"
#folderType = "NEW"
#folderName = "Test"

### ENVIRONMENT SETUP ##########################################################################################################

arcpy.env.overwriteOutput = True
arcpy.env.workspace = DEFAULT_GDB
scratchWorkspace = DEFAULT_GDB
targetPath = os.path.join(targetFolder, targetFile)

sumCountyPopCB = "global sumCountyPop; sumCountyPop = {}".format(sumCountyPop)

accumulateOverallCB = """total = 0
def accumulate(increment):
 global total
 if total:
  total += increment
 else:
  total = increment
 return total"""

accumulateByCountyCB = """total = {}
def accumulate(increment, county):
 global total
 if county in total:
  total[county] += increment
 else:
  total[county] = increment
 return total[county]"""

stripWhitespaceCB = """
def strip_whitespace(countyName):
    return countyName.rstrip()"""

### MAIN SCRIPT #############################################################################################################

###########################################################################################################################
# Download ODH data and create tables containing summary statistics
# Data source: 
#    Ohio Dept of Health
#    https://coronavirus.ohio.gov/static/dashboards/vaccine_data.csv
###########################################################################################################################

logfile = open(LOGFILE, "a")

logfile.write("{0}\tStarting script\n".format(datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S")))
logfile.flush()
os.fsync(logfile)

# Retrieve latest data from Ohio Dept of Health 
message = "Retrieve latest data from Ohio Dept of Health"
print(message)
logfile.write("{0}\t{1}\n".format(datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S"), message))
logfile.flush()
os.fsync(logfile)
timestamp = datetime.strftime(datetime.now(),'%Y%m%d%H%M%S')
try:
	urllib.request.urlretrieve(sourceUrl, os.path.join(targetFolder, targetFile))
except Exception as e:
    message = "Failed to retrieve source data. " + str(e)
    print(message)
    logfile.write("{0}\t{1}\n".format(datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S"), message))
    logfile.flush()
    os.fsync(logfile)
# Create archival copy of the downloaded data with timestamp appended to filename
try:
	shutil.copyfile(os.path.join(targetFolder, targetFile), os.path.join(targetFolder, targetFile.replace(".csv", "_" + timestamp + ".csv")))
except Exception as e:
    message = "Failed to create target files. " + str(e)
    print(message)
    logfile.write("{0}\t{1}\n".format(datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S"), message))
    logfile.flush()
    os.fsync(logfile)

message = "Loading population data"
print(message)
logfile.write("{0}\t{1}\n".format(datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S"), message))
logfile.flush()
os.fsync(logfile)
# Load CSV into table in memory (read only)
countyPopTable = arcpy.MakeTableView_management(in_table=countyPopulations, out_view=r"memory\countyPopulations", where_clause="", workspace="", field_info="county county VISIBLE NONE;pop pop VISIBLE NONE")

message = "Preparing ODH summary table"
print(message)
logfile.write("{0}\t{1}\n".format(datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S"), message))
logfile.flush()
os.fsync(logfile)
# Load CSV into table in memory (read only)
arcpy.MakeTableView_management(in_table=targetPath, out_view=r"memory\odhSummaryTemp", where_clause="", workspace="", field_info="county county VISIBLE NONE;date date VISIBLE NONE;vaccines_started vaccines_started VISIBLE NONE;vaccines_completed vaccines_completed VISIBLE NONE")

# Create a permanent, manipulatable table
odhSummary = arcpy.TableToTable_conversion(in_rows=r"memory\odhSummaryTemp", out_path=DEFAULT_GDB, out_name="odhSummary", where_clause="", field_mapping="county \"county\" true true false 8000 Text 0 0,First,#,odhSummaryTemp,county,0,8000;date \"date\" true true false 8 Date 0 0,First,#,odhSummaryTemp,date,-1,-1;vaccines_started \"vaccines_started\" true true false 4 Long 0 0,First,#,odhSummaryTemp,vaccines_started,-1,-1;vaccines_completed \"vaccines_completed\" true true false 4 Long 0 0,First,#,odhSummaryTemp,vaccines_completed,-1,-1", config_keyword="")[0]

# ArcGIS Online expects dates to be stored in UTC, but displays them to clients in their local time.
# Create new date fields to store onset date and death date in UTC.  Leave existing fields representing the dates in Eastern time as strings. 
# Hopefully users can easily work with the data by making selective use of the two sets of fields.
odhSummary = arcpy.ConvertTimeZone_management(in_table="odhSummary", input_time_field="date", input_time_zone="Eastern_Standard_Time", output_time_field="date_UTC", output_time_zone="UTC", input_dst="INPUT_ADJUSTED_FOR_DST", output_dst="OUTPUT_ADJUSTED_FOR_DST")[0]
odhSummary = arcpy.AlterField_management(in_table="odhSummary", field="date", new_field_name="date", new_field_alias="Vaccination date (Eastern)", field_type="", field_length=8, field_is_nullable="NULLABLE", clear_field_alias="DO_NOT_CLEAR")[0]
odhSummary = arcpy.AlterField_management(in_table="odhSummary", field="date_UTC", new_field_name="date_UTC", new_field_alias="Vaccination date (UTC)", field_type="", field_length=8, field_is_nullable="NULLABLE", clear_field_alias="DO_NOT_CLEAR")[0]
odhSummary = arcpy.management.CalculateField(in_table="odhSummary", field="county", expression="strip_whitespace(!county!)", expression_type="PYTHON3", code_block=stripWhitespaceCB, field_type="TEXT")[0]
tableName = "odhSummary"
if os.path.exists("{}.csv".format(tableName)):
    os.unlink("{}.csv".format(tableName))
arcpy.TableToTable_conversion(in_rows=tableName, out_path="./", out_name="{}.csv".format(tableName))[0]

message = "Summarize vaccines by county by date"
print(message)
logfile.write("{0}\t{1}\n".format(datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S"), message))
logfile.flush()
os.fsync(logfile)
VaccinesByCountyByDate = arcpy.analysis.Statistics(in_table="odhSummary", out_table="VaccinesByCountyByDate", statistics_fields=[["vaccines_started", "SUM"], ["vaccines_completed", "SUM"]], case_field=["date_UTC", "county"])
VaccinesByCountyByDate = arcpy.management.AlterField(in_table="VaccinesByCountyByDate", field="SUM_vaccines_started", new_field_name="startedToday", new_field_alias="Vaccines started today", field_type="", field_length=8, field_is_nullable="NULLABLE", clear_field_alias="DO_NOT_CLEAR")[0]
VaccinesByCountyByDate = arcpy.management.AddField(in_table="VaccinesByCountyByDate", field_name="startedTotal", field_type="LONG", field_precision=None, field_scale=None, field_length=None, field_alias="Total vaccines started", field_is_nullable="NULLABLE", field_is_required="NON_REQUIRED", field_domain="")[0]
VaccinesByCountyByDate = arcpy.management.CalculateField(in_table="VaccinesByCountyByDate", field="startedTotal", expression="accumulate(!startedToday!,!county!)", expression_type="PYTHON3", code_block=accumulateByCountyCB, field_type="TEXT")[0]
VaccinesByCountyByDate = arcpy.management.AlterField(in_table="VaccinesByCountyByDate", field="SUM_vaccines_completed", new_field_name="completedToday", new_field_alias="Vaccines completed today", field_type="", field_length=8, field_is_nullable="NULLABLE", clear_field_alias="DO_NOT_CLEAR")[0]
VaccinesByCountyByDate = arcpy.management.AddField(in_table="VaccinesByCountyByDate", field_name="completedTotal", field_type="LONG", field_precision=None, field_scale=None, field_length=None, field_alias="Total vaccines completed", field_is_nullable="NULLABLE", field_is_required="NON_REQUIRED", field_domain="")[0]
VaccinesByCountyByDate = arcpy.management.CalculateField(in_table="VaccinesByCountyByDate", field="completedTotal", expression="accumulate(!completedToday!,!county!)", expression_type="PYTHON3", code_block=accumulateByCountyCB, field_type="TEXT")[0]
VaccinesByCountyByDate = arcpy.JoinField_management(in_data="VaccinesByCountyByDate", in_field="county", join_table=countyPopTable, join_field="county", fields=["pop"])[0]
VaccinesByCountyByDate = arcpy.management.AddField(in_table="VaccinesByCountyByDate", field_name="completionRate", field_type="DOUBLE", field_precision=None, field_scale=None, field_length=None, field_alias="Percent of vaccinations completed", field_is_nullable="NULLABLE", field_is_required="NON_REQUIRED", field_domain="")[0]
VaccinesByCountyByDate = arcpy.management.CalculateField(in_table="VaccinesByCountyByDate", field="completionRate", expression="!completedTotal!/!startedTotal!", expression_type="PYTHON3")[0]
VaccinesByCountyByDate = arcpy.management.AddField(in_table="VaccinesByCountyByDate", field_name="startedPerCap", field_type="DOUBLE", field_precision=None, field_scale=None, field_length=None, field_alias="Vaccines started per capita", field_is_nullable="NULLABLE", field_is_required="NON_REQUIRED", field_domain="")[0]
VaccinesByCountyByDate = arcpy.management.CalculateField(in_table="VaccinesByCountyByDate", field="startedPerCap", expression="!startedTotal!/!pop!", expression_type="PYTHON3")[0]
VaccinesByCountyByDate = arcpy.management.AddField(in_table="VaccinesByCountyByDate", field_name="completedPerCap", field_type="DOUBLE", field_precision=None, field_scale=None, field_length=None, field_alias="Vaccines completed per capita", field_is_nullable="NULLABLE", field_is_required="NON_REQUIRED", field_domain="")[0]
VaccinesByCountyByDate = arcpy.management.CalculateField(in_table="VaccinesByCountyByDate", field="completedPerCap", expression="!completedTotal!/!pop!", expression_type="PYTHON3")[0]
VaccinesByCountyByDate = arcpy.management.DeleteField(in_table="VaccinesByCountyByDate", drop_field=["FREQUENCY","pop"])[0]
tableName = "VaccinesByCountyByDate"
if os.path.exists("{}.csv".format(tableName)):
    os.unlink("{}.csv".format(tableName))
arcpy.TableToTable_conversion(in_rows=tableName, out_path="./", out_name="{}.csv".format(tableName))[0]

message = "Summarize vaccines by county"
print(message)
logfile.write("{0}\t{1}\n".format(datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S"), message))
logfile.flush()
os.fsync(logfile)
VaccinesByCounty = arcpy.analysis.Statistics(in_table="odhSummary", out_table="VaccinesByCounty", statistics_fields=[["vaccines_started", "SUM"], ["vaccines_completed", "SUM"]], case_field=["county"])
VaccinesByCounty = arcpy.management.AlterField(in_table="VaccinesByCounty", field="SUM_vaccines_started", new_field_name="startedTotal", new_field_alias="Total vaccines started", field_type="", field_length=8, field_is_nullable="NULLABLE", clear_field_alias="DO_NOT_CLEAR")[0]
VaccinesByCounty = arcpy.management.AlterField(in_table="VaccinesByCounty", field="SUM_vaccines_completed", new_field_name="completedTotal", new_field_alias="Total vaccines completed", field_type="", field_length=8, field_is_nullable="NULLABLE", clear_field_alias="DO_NOT_CLEAR")[0]
VaccinesByCounty = arcpy.JoinField_management(in_data="VaccinesByCounty", in_field="county", join_table=countyPopTable, join_field="county", fields=["pop"])[0]
VaccinesByCounty = arcpy.management.AddField(in_table="VaccinesByCounty", field_name="completionRate", field_type="DOUBLE", field_precision=None, field_scale=None, field_length=None, field_alias="Percent of vaccinations completed", field_is_nullable="NULLABLE", field_is_required="NON_REQUIRED", field_domain="")[0]
VaccinesByCounty = arcpy.management.CalculateField(in_table="VaccinesByCounty", field="completionRate", expression="!completedTotal!/!startedTotal!", expression_type="PYTHON3")[0]
VaccinesByCounty = arcpy.management.AddField(in_table="VaccinesByCounty", field_name="startedPerCap", field_type="DOUBLE", field_precision=None, field_scale=None, field_length=None, field_alias="Vaccines started per capita", field_is_nullable="NULLABLE", field_is_required="NON_REQUIRED", field_domain="")[0]
VaccinesByCounty = arcpy.management.CalculateField(in_table="VaccinesByCounty", field="startedPerCap", expression="!startedTotal!/!pop!", expression_type="PYTHON3")[0]
VaccinesByCounty = arcpy.management.AddField(in_table="VaccinesByCounty", field_name="completedPerCap", field_type="DOUBLE", field_precision=None, field_scale=None, field_length=None, field_alias="Vaccines completed per capita", field_is_nullable="NULLABLE", field_is_required="NON_REQUIRED", field_domain="")[0]
VaccinesByCounty = arcpy.management.CalculateField(in_table="VaccinesByCounty", field="completedPerCap", expression="!completedTotal!/!pop!", expression_type="PYTHON3")[0]
VaccinesByCounty = arcpy.management.DeleteField(in_table="VaccinesByCounty", drop_field=["FREQUENCY", "pop"])[0]
tableName = "VaccinesByCounty"
if os.path.exists("{}.csv".format(tableName)):
    os.unlink("{}.csv".format(tableName))
arcpy.TableToTable_conversion(in_rows=tableName, out_path="./", out_name="{}.csv".format(tableName))[0]

message = "Summarize vaccines by date"
print(message)
logfile.write("{0}\t{1}\n".format(datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S"), message))
logfile.flush()
os.fsync(logfile)
VaccinesByDate = arcpy.analysis.Statistics(in_table="odhSummary", out_table="VaccinesByDate", statistics_fields=[["vaccines_started", "SUM"], ["vaccines_completed", "SUM"]], case_field=["date_UTC"])
VaccinesByDate = arcpy.management.AlterField(in_table="VaccinesByDate", field="SUM_vaccines_started", new_field_name="startedToday", new_field_alias="Vaccines started today", field_type="", field_length=8, field_is_nullable="NULLABLE", clear_field_alias="DO_NOT_CLEAR")[0]
VaccinesByDate = arcpy.management.AddField(in_table="VaccinesByDate", field_name="startedTotal", field_type="LONG", field_precision=None, field_scale=None, field_length=None, field_alias="Total vaccines started", field_is_nullable="NULLABLE", field_is_required="NON_REQUIRED", field_domain="")[0]
VaccinesByDate = arcpy.management.CalculateField(in_table="VaccinesByDate", field="startedTotal", expression="accumulate(!startedToday!)", expression_type="PYTHON3", code_block=accumulateOverallCB, field_type="TEXT")[0]
VaccinesByDate = arcpy.management.AlterField(in_table="VaccinesByDate", field="SUM_vaccines_completed", new_field_name="completedToday", new_field_alias="Vaccines completed today", field_type="", field_length=8, field_is_nullable="NULLABLE", clear_field_alias="DO_NOT_CLEAR")[0]
VaccinesByDate = arcpy.management.AddField(in_table="VaccinesByDate", field_name="completedTotal", field_type="LONG", field_precision=None, field_scale=None, field_length=None, field_alias="Total vaccines completed", field_is_nullable="NULLABLE", field_is_required="NON_REQUIRED", field_domain="")[0]
VaccinesByDate = arcpy.management.CalculateField(in_table="VaccinesByDate", field="completedTotal", expression="accumulate(!completedToday!)", expression_type="PYTHON3", code_block=accumulateOverallCB, field_type="TEXT")[0]
VaccinesByDate = arcpy.management.AddField(in_table="VaccinesByDate", field_name="completionRate", field_type="DOUBLE", field_precision=None, field_scale=None, field_length=None, field_alias="Percent of vaccinations completed", field_is_nullable="NULLABLE", field_is_required="NON_REQUIRED", field_domain="")[0]
VaccinesByDate = arcpy.management.CalculateField(in_table="VaccinesByDate", field="completionRate", expression="!completedTotal!/!startedTotal!", expression_type="PYTHON3")[0]
VaccinesByDate = arcpy.management.AddField(in_table="VaccinesByDate", field_name="startedPerCap", field_type="DOUBLE", field_precision=None, field_scale=None, field_length=None, field_alias="Vaccines started per capita", field_is_nullable="NULLABLE", field_is_required="NON_REQUIRED", field_domain="")[0]
VaccinesByDate = arcpy.management.CalculateField(in_table="VaccinesByDate", field="startedPerCap", expression="!startedTotal!/sumCountyPop", code_block=sumCountyPopCB, expression_type="PYTHON3")[0]
VaccinesByDate = arcpy.management.AddField(in_table="VaccinesByDate", field_name="completedPerCap", field_type="DOUBLE", field_precision=None, field_scale=None, field_length=None, field_alias="Vaccines completed per capita", field_is_nullable="NULLABLE", field_is_required="NON_REQUIRED", field_domain="")[0]
VaccinesByDate = arcpy.management.CalculateField(in_table="VaccinesByDate", field="completedPerCap", expression="!completedTotal!/sumCountyPop", code_block=sumCountyPopCB, expression_type="PYTHON3")[0]
VaccinesByDate = arcpy.management.DeleteField(in_table="VaccinesByDate", drop_field=["FREQUENCY", "SUM_vaccines_completed"])[0]
tableName = "VaccinesByDate"
if os.path.exists("{}.csv".format(tableName)):
    os.unlink("{}.csv".format(tableName))
arcpy.TableToTable_conversion(in_rows=tableName, out_path="./", out_name="{}.csv".format(tableName))[0]


###########################################################################################################################
# Join county statistics to county boundary polygons and create map layers
# Data source: 
#    U.S. Census Bureau
#    2018 American Community Survey Cartographic Boundary Shapefiles
#    https://www2.census.gov/geo/tiger/GENZ2018/shp/cb_2018_us_county_500k.zip
###########################################################################################################################

message = "Preparing county features"
print(message)
logfile.write("{0}\t{1}\n".format(datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S"), message))
logfile.flush()
os.fsync(logfile)
# Select only Ohio counties from Census cartographic boundary polygons using Ohio's state code
if arcpy.Exists("countyFeaturesSelect") and arcpy.env.overwriteOutput:
    arcpy.Delete_management("countyFeaturesSelect")
arcpy.Select_analysis(in_features=countyFeatures, out_feature_class="countyFeaturesSelect", where_clause="STATEFP = '39'")

# Reproject selected features to Web Mercator and save as a new feature class
if arcpy.Exists("OhioCounties") and arcpy.env.overwriteOutput:
    arcpy.Delete_management("OhioCounties")
arcpy.Project_management(in_dataset="countyFeaturesSelect", out_dataset="OhioCounties", out_coor_system="PROJCS['WGS_1984_Web_Mercator_Auxiliary_Sphere',GEOGCS['GCS_WGS_1984',DATUM['D_WGS_1984',SPHEROID['WGS_1984',6378137.0,298.257223563]],PRIMEM['Greenwich',0.0],UNIT['Degree',0.0174532925199433]],PROJECTION['Mercator_Auxiliary_Sphere'],PARAMETER['False_Easting',0.0],PARAMETER['False_Northing',0.0],PARAMETER['Central_Meridian',0.0],PARAMETER['Standard_Parallel_1',0.0],PARAMETER['Auxiliary_Sphere_Type',0.0],UNIT['Meter',1.0]]", transform_method=["WGS_1984_(ITRF00)_To_NAD_1983"], in_coor_system="GEOGCS['GCS_North_American_1983',DATUM['D_North_American_1983',SPHEROID['GRS_1980',6378137.0,298.257222101]],PRIMEM['Greenwich',0.0],UNIT['Degree',0.0174532925199433]]", preserve_shape="NO_PRESERVE_SHAPE", max_deviation="", vertical="NO_VERTICAL")
arcpy.Delete_management("countyFeaturesSelect")
arcpy.JoinField_management(in_data="OhioCounties", in_field="NAME", join_table="VaccinesByCounty", join_field="county", fields=["startedTotal", "completedTotal", "completionRate", "startedPerCap", "completedPerCap"])[0]
arcpy.management.DeleteField(in_table="OhioCounties", drop_field=["STATEFP", "COUNTYFP", "COUNTYNS", "AFFGEOID", "LSAD", "ALAND", "AWATER"])[0]

message = "Create vaccinations started feature layer"
print(message)
logfile.write("{0}\t{1}\n".format(datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S"), message))
logfile.flush()
os.fsync(logfile)
arcpy.MakeFeatureLayer_management(in_features="OhioCounties", out_layer="Total vaccinations started", where_clause="", workspace="", field_info="OBJECTID OBJECTID VISIBLE NONE;Shape Shape VISIBLE NONE;STATEFP STATEFP VISIBLE NONE;COUNTYFP COUNTYFP VISIBLE NONE;COUNTYNS COUNTYNS VISIBLE NONE;AFFGEOID AFFGEOID VISIBLE NONE;GEOID GEOID VISIBLE NONE;NAME NAME VISIBLE NONE;LSAD LSAD VISIBLE NONE;ALAND ALAND VISIBLE NONE;AWATER AWATER VISIBLE NONE;Shape_length Shape_length VISIBLE NONE;Shape_area Shape_area VISIBLE NONE;Shape_length Shape_length VISIBLE NONE;Shape_area Shape_area VISIBLE NONE")
arcpy.ApplySymbologyFromLayer_management(in_layer="Total vaccinations started", in_symbology_layer="./Total vaccinations started.lyrx", symbology_fields=[["VALUE_FIELD", "startedTotal", "startedTotal"]], update_symbology="UPDATE")[0]

message = "Create vaccinations started per capita feature layer"
print(message)
logfile.write("{0}\t{1}\n".format(datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S"), message))
logfile.flush()
os.fsync(logfile)
arcpy.MakeFeatureLayer_management(in_features="OhioCounties", out_layer="Vaccinations started per capita", where_clause="", workspace="", field_info="OBJECTID OBJECTID VISIBLE NONE;Shape Shape VISIBLE NONE;STATEFP STATEFP VISIBLE NONE;COUNTYFP COUNTYFP VISIBLE NONE;COUNTYNS COUNTYNS VISIBLE NONE;AFFGEOID AFFGEOID VISIBLE NONE;GEOID GEOID VISIBLE NONE;NAME NAME VISIBLE NONE;LSAD LSAD VISIBLE NONE;ALAND ALAND VISIBLE NONE;AWATER AWATER VISIBLE NONE;Shape_length Shape_length VISIBLE NONE;Shape_area Shape_area VISIBLE NONE;Shape_length Shape_length VISIBLE NONE;Shape_area Shape_area VISIBLE NONE")
arcpy.ApplySymbologyFromLayer_management(in_layer="Vaccinations started per capita", in_symbology_layer="./Vaccinations started per capita.lyrx", symbology_fields=[["VALUE_FIELD", "startedPerCap", "startedPerCap"]], update_symbology="UPDATE")[0]

message = "Create vaccinations completed feature layer"
print(message)
logfile.write("{0}\t{1}\n".format(datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S"), message))
logfile.flush()
os.fsync(logfile)
arcpy.MakeFeatureLayer_management(in_features="OhioCounties", out_layer="Total vaccinations completed", where_clause="", workspace="", field_info="OBJECTID OBJECTID VISIBLE NONE;Shape Shape VISIBLE NONE;STATEFP STATEFP VISIBLE NONE;COUNTYFP COUNTYFP VISIBLE NONE;COUNTYNS COUNTYNS VISIBLE NONE;AFFGEOID AFFGEOID VISIBLE NONE;GEOID GEOID VISIBLE NONE;NAME NAME VISIBLE NONE;LSAD LSAD VISIBLE NONE;ALAND ALAND VISIBLE NONE;AWATER AWATER VISIBLE NONE;Shape_length Shape_length VISIBLE NONE;Shape_area Shape_area VISIBLE NONE;Shape_length Shape_length VISIBLE NONE;Shape_area Shape_area VISIBLE NONE")
arcpy.ApplySymbologyFromLayer_management(in_layer="Total vaccinations completed", in_symbology_layer="./Total vaccinations completed.lyrx", symbology_fields=[["VALUE_FIELD", "completedTotal", "completedTotal"]], update_symbology="UPDATE")[0]

message = "Create vaccinations completed per capita feature layer"
print(message)
logfile.write("{0}\t{1}\n".format(datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S"), message))
logfile.flush()
os.fsync(logfile)
arcpy.MakeFeatureLayer_management(in_features="OhioCounties", out_layer="Vaccinations completed per capita", where_clause="", workspace="", field_info="OBJECTID OBJECTID VISIBLE NONE;Shape Shape VISIBLE NONE;STATEFP STATEFP VISIBLE NONE;COUNTYFP COUNTYFP VISIBLE NONE;COUNTYNS COUNTYNS VISIBLE NONE;AFFGEOID AFFGEOID VISIBLE NONE;GEOID GEOID VISIBLE NONE;NAME NAME VISIBLE NONE;LSAD LSAD VISIBLE NONE;ALAND ALAND VISIBLE NONE;AWATER AWATER VISIBLE NONE;Shape_length Shape_length VISIBLE NONE;Shape_area Shape_area VISIBLE NONE;Shape_length Shape_length VISIBLE NONE;Shape_area Shape_area VISIBLE NONE")
arcpy.ApplySymbologyFromLayer_management(in_layer="Vaccinations completed per capita", in_symbology_layer="./Vaccinations completed per capita.lyrx", symbology_fields=[["VALUE_FIELD", "completedPerCap", "completedPerCap"]], update_symbology="UPDATE")[0]

message = "Create vaccination completion rate layer"
print(message)
logfile.write("{0}\t{1}\n".format(datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S"), message))
logfile.flush()
os.fsync(logfile)
arcpy.MakeFeatureLayer_management(in_features="OhioCounties", out_layer="Vaccination completion rate", where_clause="", workspace="", field_info="OBJECTID OBJECTID VISIBLE NONE;Shape Shape VISIBLE NONE;STATEFP STATEFP VISIBLE NONE;COUNTYFP COUNTYFP VISIBLE NONE;COUNTYNS COUNTYNS VISIBLE NONE;AFFGEOID AFFGEOID VISIBLE NONE;GEOID GEOID VISIBLE NONE;NAME NAME VISIBLE NONE;LSAD LSAD VISIBLE NONE;ALAND ALAND VISIBLE NONE;AWATER AWATER VISIBLE NONE;Shape_length Shape_length VISIBLE NONE;Shape_area Shape_area VISIBLE NONE;Shape_length Shape_length VISIBLE NONE;Shape_area Shape_area VISIBLE NONE")
arcpy.ApplySymbologyFromLayer_management(in_layer="Vaccination completion rate", in_symbology_layer="./Vaccination completion rate.lyrx", symbology_fields=[["VALUE_FIELD", "completionRate", "completionRate"]], update_symbology="UPDATE")[0]

message = "Create county boundaries feature layer"
print(message)
logfile.write("{0}\t{1}\n".format(datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S"), message))
logfile.flush()
os.fsync(logfile)
arcpy.MakeFeatureLayer_management(in_features="OhioCounties", out_layer="Counties", where_clause="", workspace="", field_info="OBJECTID OBJECTID VISIBLE NONE;Shape Shape VISIBLE NONE;STATEFP STATEFP VISIBLE NONE;COUNTYFP COUNTYFP VISIBLE NONE;COUNTYNS COUNTYNS VISIBLE NONE;AFFGEOID AFFGEOID VISIBLE NONE;GEOID GEOID VISIBLE NONE;NAME NAME VISIBLE NONE;LSAD LSAD VISIBLE NONE;ALAND ALAND VISIBLE NONE;AWATER AWATER VISIBLE NONE;Shape_length Shape_length VISIBLE NONE;Shape_area Shape_area VISIBLE NONE;Shape_length Shape_length VISIBLE NONE;Shape_area Shape_area VISIBLE NONE")
arcpy.ApplySymbologyFromLayer_management(in_layer="Counties", in_symbology_layer="./Counties.lyrx", symbology_fields=[], update_symbology="DEFAULT")[0]


###########################################################################################################################
# Publish layers and tables to ArcGIS Online
###########################################################################################################################

if folderOverride == False:
    message = "Using service definition from draft (no override)"
    print(message)
    logfile.write("{0}\t{1}\n".format(datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S"), message))
    logfile.flush()
    os.fsync(logfile)
    folderType = "FROM_SERVICE_DEFINITION"
    folderName = ""
else:
    message = "Overriding service definition. folderType = {0}, folderName = {1}".format(folderType, folderName)
    print(message)
    logfile.write("{0}\t{1}\n".format(datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S"), message))
    logfile.flush()
    os.fsync(logfile)
message = "Staging service"
print(message)
logfile.write("{0}\t{1}\n".format(datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S"), message))
logfile.flush()
os.fsync(logfile)
try:
    serviceDefinition = arcpy.StageService_server(in_service_definition_draft=serviceDraft, out_service_definition=None)
    warnings = arcpy.GetMessages(1)
    print(warnings)
except Exception as stage_exception:
    message = "Sddraft not staged. Analyzer errors encountered - {}".format(str(stage_exception))
    print(message)
    logfile.write("{0}\t{1}\n".format(datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S"), message))
    logfile.flush()
    os.fsync(logfile)
    sys.exit(1)

message = "Uploading service"
print(message)
logfile.write("{0}\t{1}\n".format(datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S"), message))
logfile.flush()
os.fsync(logfile)
try:
    arcpy.UploadServiceDefinition_server(in_sd_file=serviceDefinition, in_server="My Hosted Services", in_service_name="", in_cluster="", in_folder_type=folderType, in_folder=folderName, in_startupType="STARTED", in_override="USE_DEFINITION", in_my_contents="NO_SHARE_ONLINE", in_public="PRIVATE", in_organization="NO_SHARE_ORGANIZATION", in_groups=[])
except Exception as upload_exception:
    message = "Service not uploaded - {}".format(str(upload_exception))
    print(message)
    logfile.write("{0}\t{1}\n".format(datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S"), message))
    logfile.flush()
    os.fsync(logfile)
    sys.exit(1)
	
message = "Script completed successfully"
print(message)
logfile.write("{0}\t{1}\n".format(datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S"), message))
logfile.flush()
os.fsync(logfile)
logfile.close()

sys.exit(0)