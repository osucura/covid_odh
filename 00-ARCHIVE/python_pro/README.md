# COVID-19 Ohio County-Level Data - Python Scripts for ArcGIS Pro

## Introduction

There are two Python scripts (and their support files) contained herein:
  - `cases\updateWebLayers.py` will retrieve [COVID-19 case data](https://coronavirus.ohio.gov/static/dashboards/COVIDDeathData_CountyOfResidence.csv) from Ohio Department of Health, summarize it in various ways, and publish an ArcGIS Online feature layer comprised of a set of feature services and tables.
  - `vaccines\updateWebLayers.py` will retrieve [COVID-19 vaccination data](https://coronavirus.ohio.gov/static/dashboards/vaccine_data.csv) from Ohio Department of Health, summarize it in various ways, and publish an ArcGIS Online feature layer comprised of a set of feature services and tables.

## Instructions

As of 4/3/2020, the process is fully automated after a set of manual prerequisite steps:
  1. Create an empty file geodatabase in the same folder as the script (default: `Default.gdb`)
  2. Create a service draft.  One way to do this is to run the ModelBuilder scripts and manually publish the service, as described in the README.md for the ModelBuilder scripts.
  3. Update the service draft path in the appropriate script to point to the service draft on your machine.  For example:

  ```
  serviceDraft = os.path.normpath(r"C:\Users\porr.4\Desktop\covid-ohio\covid-ohio\COVID19_in_Ohio.sddraft")
  ```  
  (Note: I could not get the `arcpy.StageService_server` function to work when I used a relative path to the service draft, hence the absolute path.)

  4. *Optional:*  Update the `folderOverride`, `folderType`, and `folderName` variables as needed to put the service where you want it in your ArcGIS Online content.

  In the terminal, change to the directory where the ```updateWebLayers.py``` script resides, then run the script as follows:

```
  "c:\Program Files\ArcGIS\Pro\bin\Python\Scripts\propy.bat" updateWebLayers.py
```

## License

[MIT License](https://opensource.org/licenses/MIT)

Copyright 2021 The Ohio State University

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
