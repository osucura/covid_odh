# COVID-19 Ohio County-Level Data

## Overview

The scripts contained herein will retrieve [COVID-19 case data](https://coronavirus.ohio.gov/static/COVIDSummaryData.csv) from Ohio Department of Health, summarize it in various ways, and publish an ArcGIS Online feature layer comprised of a set of feature services and tables.

The repository includes the following:
  - ArcGIS Pro ModelBuilder toolbox (see `modelbuilder_pro` folder)
  - ArcGIS Pro arcpy scripts (see `python_pro` folder)
  - Utility scripts (see `utility` folder), including a standalone Python script to retrieve and archive ODH CSV file

The `python_pro` scripts were originally derived from the `modelbuilder_pro` scripts (models), however they are maintained separately and their behaviors may diverge.

*CAVEAT: These scripts immature and are released as-is in the hope that they will be useful to other researchers and GIS users participating in the COVID-19 response in Ohio.  They work for us.  They may or may not work for you.  They will change frequently.  Use at your own risk.*

## Roadmap

As noted above, these scripts are being released prematurely to help other users.  The following improvements are planned:
  - Reduce input geometry files to include only Ohio Counties (or possibly retrieve geometry from a web service)
  - Eliminate manual project setup steps
  - Eliminate manual service draft creation steps

## Feature layer details

The original target of the scripts is the following ArcGIS Online feature layer, which is maintained by the Center for Urban and Regional Analysis (CURA).  It is our intent to update this service daily at 3pm, following the 2pm update from Ohio Department of Health.

  - ArcGIS Online Feature Layer:  http://www.arcgis.com/home/item.html?id=56132fc10d04421f917bf3c466c0216a
  - ArcGIS REST API Feature Service:  https://services6.arcgis.com/tuxY7TQIaDhLWARO/arcgis/rest/services/COVID19_in_Ohio/FeatureServer

*CAVEAT: CURA accepts no responsibility for the accuracy of the original Ohio Department of Health Data or for the fidelity of the representation of that data provided by the aforementioned services.  CURA accepts no responsibility for the availability of these services.  The services are intended for research use only. Use at your own risk.*

The contents of the feature service are as follows:
  - Layers:
    - Counties
    - Total cases
  - Tables
    - odhSummary
    - TotalCasesByCountyBySex
    - CasesByCountyByDate
    - DeathsByCountyByDate
    - TotalCasesByCountyByAge
    - TotalCasesByCounty
    - TotalDeathsByCounty
    - CasesByDate
    - DeathsByDate
    - TotalHospitalizationsByCounty

Notes:
  - The spatial layers (`Counties`, `Total Cases`) include county boundary geometry from the [2018 American Community Survey Cartographic Boundary Shapefiles](https://www2.census.gov/geo/tiger/GENZ2018/shp/cb_2018_us_county_500k.zip) (U.S. Census Bureau)
  - The fields from tables `TotalCasesByCounty`, `TotalDeathsByCounty`, `TotalHospitalizationsByCounty`, `TotalCasesByCountyByAge`, and `TotalCasesByCountyBySex` have been joined to the spatial layers.
  - The `odhSummary` table is a direct copy of the raw data from ODH with two exceptions:
    - The final row has been removed.  This line contained column totals.
    - Two fields have been added (`Onset_Case_UTC`, `Date_Of_Death_UTC`).  These provide a representation of the onset date and date of death associated with each case as a Date type and converted to UTC time.  This is necessary for to allow for proper filtering and sorting of dates in ArcGIS Online web applications.

## License

[MIT License](https://opensource.org/licenses/MIT)

Copyright 2020 The Ohio State University

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
