# downloadODHSummaryStandalone.py
# Source: COVID-19 Ohio County-Level Data (https://gitlab.com/osucura/covid_odh)
# Author: Center for Urban and Regional Analysis
# License: MIT License
# Copyright 2020 The Ohio State University

import sys
import urllib.request
import os.path
import shutil
from datetime import datetime

try:
	targetPath = os.path.normpath(sys.argv[1])
except:
	targetPath = os.path.normpath("./COVIDSummaryData.csv")

timestamp = datetime.strftime(datetime.now(),'%Y%m%d%H%M%S')

sourceUrl = "https://coronavirus.ohio.gov/static/COVIDSummaryData.csv"

try:
	urllib.request.urlretrieve(sourceUrl, targetPath)
except Exception as e:
	print("Failed to retrieve source data. " + str(e) )

try:
	shutil.copyfile(targetPath, targetPath.replace(".csv", "_" + timestamp + ".csv"))
except Exception as e:
	print("Failed to create target files. " + str(e) )
