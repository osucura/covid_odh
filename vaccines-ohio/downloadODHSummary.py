from arcpy import GetParameterAsText
import urllib.request
import os.path
import shutil
from datetime import datetime

targetFile = "COVIDSummaryData.csv"

timestamp = datetime.strftime(datetime.now(),'%Y%m%d%H%M%S')

sourceUrl = GetParameterAsText(0)
if(sourceUrl == ""):
	sourceUrl = "https://coronavirus.ohio.gov/static/COVIDSummaryData.csv"

targetFolder = GetParameterAsText(1)
if(targetFolder == ""):
	targetFolder = os.path.normpath("./input_data/")

try:
	urllib.request.urlretrieve(sourceUrl, os.path.join(targetFolder, targetFile))
except Exception as e:
	print("Failed to retrieve source data. " + str(e) )

try:
	shutil.copyfile(os.path.join(targetFolder, targetFile), os.path.join(targetFolder, targetFile.replace(".csv", "_" + timestamp + ".csv")))
except Exception as e:
	print("Failed to create target files. " + str(e) )
