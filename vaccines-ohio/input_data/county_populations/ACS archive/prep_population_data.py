import pandas as pd

df = pd.read_csv("ACSDT5Y2019.B01003_data_with_overlays_2021-03-01T152425.csv", skiprows=[1])
df["county"] = df["NAME"].apply(lambda x:x.replace(" County, Ohio",""))
df["pop"] = df["B01003_001E"]
df = df[["county", "pop"]]
df.to_csv("county_populations.csv", index=False)