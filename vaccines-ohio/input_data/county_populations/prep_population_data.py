import pandas as pd

df = pd.read_csv("ohio_pop_demographics_2019.csv")
df = df[["CTYNAME", "TOT_POP"]].copy()
df["county"] = df["CTYNAME"].apply(lambda x:x.replace(" County",""))
df = df.groupby(by=["county"], axis=0).sum().copy()
df.reset_index(inplace=True)
df["pop"] = df["TOT_POP"]
df = df[["county", "pop"]]
df.loc[len(df.index)] = ["Unknown", None]
df.loc[len(df.index)] = ["Out of State", None]
df.to_csv("county_populations.csv", index=False)