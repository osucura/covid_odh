# updateWebLayers.py
# Source: COVID-19 Ohio County-Level Data (https://gitlab.com/osucura/covid_odh)
# Author: Center for Urban and Regional Analysis
# License: MIT License
# Copyright 2020 The Ohio State University

import arcpy
import sys
import os.path
import urllib.request
import shutil
from datetime import datetime

# Script configuration
LOGFILE = os.path.normpath("./updateWebLayers.log")
DEFAULT_GDB = os.path.normpath("./Default.gdb")
agoUser = "curaCovid"
agoPassword = "MgWCKBEiJ5K2mnErcCsV"
targetFile = "COVIDSummaryData.csv"
targetFolder = os.path.normpath("./input_data")
sourceUrl = "https://coronavirus.ohio.gov/static/dashboards/COVIDDeathData_CountyOfResidence.csv"
countyFeatures = os.path.normpath("./input_data/cb_2018_us_county_500k/cb_2018_us_county_500k.shp")
casesLayerFile = os.path.normpath("./Total cases (graduated).lyrx")
boundariesLayerFile = os.path.normpath("./Counties.lyrx")
serviceDraft = os.path.normpath(r"C:\temp\covid-ohio\covid-ohio\COVID19_in_Ohio.sddraft")
folderOverride = False
#folderType = "EXISTING"
#folderType = "NEW"
#folderName = "Test"

### ENVIRONMENT SETUP ##########################################################################################################

arcpy.env.overwriteOutput = True
arcpy.env.workspace = DEFAULT_GDB
scratchWorkspace = DEFAULT_GDB
targetPath = os.path.join(targetFolder, targetFile)

accumulateOverallCB = """total = 0
def accumulate(increment):
 global total
 if total:
  total += increment
 else:
  total = increment
 return total"""

accumulateByCountyCB = """total = {}
def accumulate(increment, county):
 global total
 if county in total:
  total[county] += increment
 else:
  total[county] = increment
 return total[county]"""

normNoData = """
def normNoData(value):
 if(value == "Unknown"):
  value = None
 return value"""

### MAIN SCRIPT #############################################################################################################

###########################################################################################################################
# Download ODH data and create tables containing summary statistics
###########################################################################################################################

logfile = open(LOGFILE, "a")

logfile.write("{0}\tStarting script\n".format(datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S")))

timestamp = datetime.strftime(datetime.now(),'%Y%m%d%H%M%S')

message = "Signing into ArcGIS Online"
print(message)
logfile.write("{0}\t{1}\n".format(datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S"), message))
try:
    arcpy.SignInToPortal("https://ohiostate.maps.arcgis.com", agoUser, agoPassword)
except Exception as e:
    message = "Failed to sign in to ArcGIS Online. " + str(e)
    print(message)
    logfile.write("{0}\t{1}\n".format(datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S"), message))

# Retrieve latest data from Ohio Dept of Health 
message = "Retrieve latest data from Ohio Dept of Health"
print(message)
logfile.write("{0}\t{1}\n".format(datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S"), message))
try:
	urllib.request.urlretrieve(sourceUrl, os.path.join(targetFolder, targetFile))
except Exception as e:
    message = "Failed to retrieve source data. " + str(e)
    print(message)
    logfile.write("{0}\t{1}\n".format(datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S"), message))

# Create archival copy of the downloaded data with timestamp appended to filename
try:
	shutil.copyfile(os.path.join(targetFolder, targetFile), os.path.join(targetFolder, targetFile.replace(".csv", "_" + timestamp + ".csv")))
except Exception as e:
    message = "Failed to create target files. " + str(e)
    print(message)
    logfile.write("{0}\t{1}\n".format(datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S"), message))

message = "Preparing ODH summary table"
print(message)
logfile.write("{0}\t{1}\n".format(datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S"), message))
# Load CSV into table in memory (read only)
arcpy.MakeTableView_management(in_table=targetPath, out_view="odhSummaryTemp", where_clause="", workspace="", field_info="")

# Remove the last line of the table, which contains pre-computed summary data
arcpy.TableSelect_analysis(in_table="odhSummaryTemp", out_table=r"memory\odhSummaryTempTruncated", where_clause="County <> 'Grand Total'")

# Standardize "no data" values for Date_Of_Death to None type
# Starting 4/22/2020, Date_Of_Death was recorded as "Unknown" in some instances.  Previously, unknown values were
# expressed as empty strings (interpreted by arcpy as None type). The string "Unknown" cannot be recast as Date.
arcpy.CalculateField_management(in_table=r"memory\odhSummaryTempTruncated", field="Date_Of_Death", expression="normNoData(!Date_Of_Death!)", expression_type="PYTHON3", code_block=normNoData)

# Create a permanent, manipulatable table
# Starting 4/22/2020, a field "Admission Date" was added, however this field is not included in the output
# Starting 8/28/2020, the field name "Death Count" changed to "Death Due to Illness Count". 
# Starting around the start of 3/2021, the field name "Death Count Due to Illness" changed to "Death Due To Illness Count - County Of Residence"
odhSummary = arcpy.TableToTable_conversion(in_rows=r"memory\odhSummaryTempTruncated", out_path=DEFAULT_GDB, out_name="odhSummary", where_clause="", field_mapping="County \"County\" true true false 8000 Text 0 0,First,#,memory\odhSummaryTempTruncated,County,0,8000;Sex \"Sex\" true true false 8000 Text 0 0,First,#,memory\odhSummaryTempTruncated,Sex,0,8000;Age_Range \"Age Range\" true true false 8000 Text 0 0,First,#,memory\odhSummaryTempTruncated,Age_Range,0,8000;Onset_Date \"Onset Date\" true true false 8000 Date 0 0,First,#,memory\odhSummaryTempTruncated,Onset_Date,0,8000;Date_Of_Death \"Date Of Death\" true true false 8000 Date 0 0,First,#,memory\odhSummaryTempTruncated,Date_Of_Death,0,8000;Case_Count \"Case Count\" true true false 4 Long 0 0,First,#,memory\odhSummaryTempTruncated,Case_Count,-1,-1;Death_Count \"Death Count\" true true false 4 Long 0 0,First,#,memory\odhSummaryTempTruncated,Death_Due_To_Illness_Count___County_Of_Residence,-1,-1;Hospitalized_Count \"Hospitalized Count\" true true false 4 Long 0 0,First,#,memory\odhSummaryTempTruncated,Hospitalized_Count,-1,-1", config_keyword="")[0]
arcpy.Delete_management(r"memory\odhSummaryTempTruncated")

# ArcGIS Online expects dates to be stored in UTC, but displays them to clients in their local time.
# Create new date fields to store onset date and death date in UTC.  Leave existing fields representing the dates in Eastern time as strings. 
# Hopefully users can easily work with the data by making selective use of the two sets of fields.
odhSummary = arcpy.ConvertTimeZone_management(in_table=odhSummary, input_time_field="Date_Of_Death", input_time_zone="Eastern_Standard_Time", output_time_field="Date_Of_Death_UTC", output_time_zone="UTC", input_dst="INPUT_ADJUSTED_FOR_DST", output_dst="OUTPUT_ADJUSTED_FOR_DST")[0]
odhSummary = arcpy.ConvertTimeZone_management(in_table=odhSummary, input_time_field="Onset_Date", input_time_zone="Eastern_Standard_Time", output_time_field="Onset_Date_UTC", output_time_zone="UTC", input_dst="INPUT_ADJUSTED_FOR_DST", output_dst="OUTPUT_ADJUSTED_FOR_DST")[0]
odhSummary = arcpy.AlterField_management(in_table=odhSummary, field="Onset_Date_UTC", new_field_name="Onset_Date_UTC", new_field_alias="Onset Date (UTC)", field_type="", field_length=8, field_is_nullable="NULLABLE", clear_field_alias="DO_NOT_CLEAR")[0]
odhSummary = arcpy.AlterField_management(in_table=odhSummary, field="Date_Of_Death_UTC", new_field_name="Date_Of_Death_UTC", new_field_alias="Date Of Death (UTC)", field_type="", field_length=8, field_is_nullable="NULLABLE", clear_field_alias="DO_NOT_CLEAR")[0]

# Summarize cases by sex by county
message = "Summarize cases by sex by county"
print(message)
logfile.write("{0}\t{1}\n".format(datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S"), message))
arcpy.PivotTable_management(in_table=odhSummary, fields=["County"], pivot_field="Sex", value_field="Case_Count", out_table=r"memory\PivotBySex")
TotalCasesByCountyBySex = arcpy.Statistics_analysis(in_table=r"memory\PivotBySex", out_table="TotalCasesByCountyBySex", statistics_fields=[["Female", "SUM"], ["Male", "SUM"], ["Unknown", "SUM"]], case_field=["County"])
TotalCasesByCountyBySex = arcpy.AlterField_management(in_table=TotalCasesByCountyBySex, field="SUM_Female", new_field_name="Female", new_field_alias="Female", field_type="", field_length=8, field_is_nullable="NULLABLE", clear_field_alias="DO_NOT_CLEAR")[0]
TotalCasesByCountyBySex = arcpy.AlterField_management(in_table=TotalCasesByCountyBySex, field="SUM_Male", new_field_name="Male", new_field_alias="Male", field_type="", field_length=8, field_is_nullable="NULLABLE", clear_field_alias="DO_NOT_CLEAR")[0]
TotalCasesByCountyBySex = arcpy.AlterField_management(in_table=TotalCasesByCountyBySex, field="SUM_Unknown", new_field_name="Unknown", new_field_alias="Sex unknown", field_type="", field_length=8, field_is_nullable="NULLABLE", clear_field_alias="DO_NOT_CLEAR")[0]
TotalCasesByCountyBySex = arcpy.DeleteField_management(in_table=TotalCasesByCountyBySex, drop_field=["FREQUENCY"])[0]
arcpy.Delete_management(r"memory\PivotBySex")

# Summarize cases by county by date
message = "Summarize cases by county by date"
print(message)
logfile.write("{0}\t{1}\n".format(datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S"), message))
CasesByCountyByDate = arcpy.Statistics_analysis(in_table=odhSummary, out_table="CasesByCountyByDate", statistics_fields=[["Case_Count", "SUM"]], case_field=["Onset_Date_UTC", "County"])
CasesByCountyByDate = arcpy.AlterField_management(in_table=CasesByCountyByDate, field="SUM_Case_Count", new_field_name="NewCases", new_field_alias="New cases", field_type="", field_length=8, field_is_nullable="NULLABLE", clear_field_alias="DO_NOT_CLEAR")[0]
CasesByCountyByDate = arcpy.AddField_management(in_table=CasesByCountyByDate, field_name="TotalCases", field_type="LONG", field_precision=None, field_scale=None, field_length=None, field_alias="Total cases", field_is_nullable="NULLABLE", field_is_required="NON_REQUIRED", field_domain="")[0]
CasesByCountyByDate = arcpy.CalculateField_management(in_table=CasesByCountyByDate, field="TotalCases", expression="accumulate(!NewCases!,!County!)", expression_type="PYTHON3", code_block=accumulateByCountyCB, field_type="TEXT")[0]
CasesByCountyByDate = arcpy.DeleteField_management(in_table=CasesByCountyByDate, drop_field=["FREQUENCY"])[0]

# Summarize deaths by county by date
message = "Summarize deaths by county by date"
print(message)
logfile.write("{0}\t{1}\n".format(datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S"), message))
DeathsByCountyByDate = arcpy.Statistics_analysis(in_table=odhSummary, out_table="DeathsByCountyByDate", statistics_fields=[["Death_Count", "SUM"]], case_field=["County", "Date_Of_Death_UTC"])
DeathsByCountyByDate = arcpy.AlterField_management(in_table=DeathsByCountyByDate, field="SUM_Death_Count", new_field_name="NewDeaths", new_field_alias="New deaths", field_type="", field_length=8, field_is_nullable="NULLABLE", clear_field_alias="DO_NOT_CLEAR")[0]
DeathsByCountyByDate = arcpy.AddField_management(in_table=DeathsByCountyByDate, field_name="TotalDeaths", field_type="LONG", field_precision=None, field_scale=None, field_length=None, field_alias="Total deaths", field_is_nullable="NULLABLE", field_is_required="NON_REQUIRED", field_domain="")[0]
DeathsByCountyByDate = arcpy.CalculateField_management(in_table=DeathsByCountyByDate, field="TotalDeaths", expression="accumulate(!NewDeaths!,!County!)", expression_type="PYTHON3", code_block=accumulateByCountyCB, field_type="TEXT")[0]
DeathsByCountyByDate = arcpy.DeleteField_management(in_table=DeathsByCountyByDate, drop_field=["FREQUENCY"])[0]

# Summarize cases by age by county
message = "Summarize cases by age by county"
print(message)
logfile.write("{0}\t{1}\n".format(datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S"), message))
arcpy.PivotTable_management(in_table=odhSummary, fields=["County"], pivot_field="Age_Range", value_field="Case_Count", out_table=r"memory\PivotByAge")
TotalCasesByCountyByAge = arcpy.Statistics_analysis(in_table=r"memory\PivotByAge", out_table="TotalCasesByCountyByAge", statistics_fields=[["F0_19", "SUM"], ["F20_29", "SUM"], ["F30_39", "SUM"], ["F40_49", "SUM"], ["F50_59", "SUM"], ["F60_69", "SUM"], ["F70_79", "SUM"], ["F80_", "SUM"]], case_field=["County"])
TotalCasesByCountyByAge = arcpy.AlterField_management(in_table=TotalCasesByCountyByAge, field="SUM_F0_19", new_field_name="AGE_0_19", new_field_alias="Age 0-19", field_type="", field_length=8, field_is_nullable="NULLABLE", clear_field_alias="DO_NOT_CLEAR")[0]
TotalCasesByCountyByAge = arcpy.AlterField_management(in_table=TotalCasesByCountyByAge, field="SUM_F20_29", new_field_name="AGE_20_29", new_field_alias="Age 20-29", field_type="", field_length=8, field_is_nullable="NULLABLE", clear_field_alias="DO_NOT_CLEAR")[0]
TotalCasesByCountyByAge = arcpy.AlterField_management(in_table=TotalCasesByCountyByAge, field="SUM_F30_39", new_field_name="AGE_30_39", new_field_alias="Age 30-39", field_type="", field_length=8, field_is_nullable="NULLABLE", clear_field_alias="DO_NOT_CLEAR")[0]
TotalCasesByCountyByAge = arcpy.AlterField_management(in_table=TotalCasesByCountyByAge, field="SUM_F40_49", new_field_name="AGE_40_49", new_field_alias="Age 40-49", field_type="", field_length=8, field_is_nullable="NULLABLE", clear_field_alias="DO_NOT_CLEAR")[0]
TotalCasesByCountyByAge = arcpy.AlterField_management(in_table=TotalCasesByCountyByAge, field="SUM_F50_59", new_field_name="AGE_50_59", new_field_alias="Age 50-59", field_type="", field_length=8, field_is_nullable="NULLABLE", clear_field_alias="DO_NOT_CLEAR")[0]
TotalCasesByCountyByAge = arcpy.AlterField_management(in_table=TotalCasesByCountyByAge, field="SUM_F60_69", new_field_name="AGE_60_69", new_field_alias="Age 60-79", field_type="", field_length=8, field_is_nullable="NULLABLE", clear_field_alias="DO_NOT_CLEAR")[0]
TotalCasesByCountyByAge = arcpy.AlterField_management(in_table=TotalCasesByCountyByAge, field="SUM_F70_79", new_field_name="AGE_70_79", new_field_alias="Age 70-79", field_type="", field_length=8, field_is_nullable="NULLABLE", clear_field_alias="DO_NOT_CLEAR")[0]
TotalCasesByCountyByAge = arcpy.AlterField_management(in_table=TotalCasesByCountyByAge, field="SUM_F80_", new_field_name="AGE_80+", new_field_alias="Age 80+", field_type="", field_length=8, field_is_nullable="NULLABLE", clear_field_alias="DO_NOT_CLEAR")[0]
TotalCasesByCountyByAge = arcpy.DeleteField_management(in_table=TotalCasesByCountyByAge, drop_field=["FREQUENCY"])[0]
arcpy.Delete_management(r"memory\PivotByAge")

# Summarize total cases by county
message = "Summarize total cases by county"
print(message)
logfile.write("{0}\t{1}\n".format(datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S"), message))
arcpy.Statistics_analysis(in_table=odhSummary, out_table="TotalCasesByCounty", statistics_fields=[["Case_Count", "SUM"]], case_field=["County"])
TotalCasesByCounty = arcpy.AlterField_management(in_table="TotalCasesByCounty", field="SUM_Case_Count", new_field_name="TotalCases", new_field_alias="Total cases", field_type="", field_length=8, field_is_nullable="NULLABLE", clear_field_alias="DO_NOT_CLEAR")[0]
TotalCasesByCounty = arcpy.DeleteField_management(in_table=TotalCasesByCounty, drop_field=["FREQUENCY"])[0]

# Summarize total deaths by county
message = "Summarize total deaths by county"
print(message)
logfile.write("{0}\t{1}\n".format(datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S"), message))
arcpy.Statistics_analysis(in_table=odhSummary, out_table="TotalDeathsByCounty", statistics_fields=[["Death_Count", "SUM"]], case_field=["County"])
TotalDeathsByCounty = arcpy.AlterField_management(in_table="TotalDeathsByCounty", field="SUM_Death_Count", new_field_name="TotalDeaths", new_field_alias="Total deaths", field_type="", field_length=8, field_is_nullable="NULLABLE", clear_field_alias="DO_NOT_CLEAR")[0]
TotalDeathsByCounty = arcpy.DeleteField_management(in_table=TotalDeathsByCounty, drop_field=["FREQUENCY"])[0]

# Summarize total hospitalizations by county
message = "Summarize hospitalizations by county"
print(message)
logfile.write("{0}\t{1}\n".format(datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S"), message))
arcpy.Statistics_analysis(in_table=odhSummary, out_table="TotalHospitalizationsByCounty", statistics_fields=[["Hospitalized_Count", "SUM"]], case_field=["County"])
TotalHospitalizationsByCounty = arcpy.AlterField_management(in_table="TotalHospitalizationsByCounty", field="SUM_Hospitalized_Count", new_field_name="TotalHospitalizations", new_field_alias="Total hospitalizations", field_type="", field_length=8, field_is_nullable="NULLABLE", clear_field_alias="DO_NOT_CLEAR")[0]
TotalDeathsByCounty = arcpy.DeleteField_management(in_table=TotalHospitalizationsByCounty, drop_field=["FREQUENCY"])[0]

# Summarize cases by date
message = "Summarize cases by date"
print(message)
logfile.write("{0}\t{1}\n".format(datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S"), message))
arcpy.Statistics_analysis(in_table=odhSummary, out_table="CasesByDate", statistics_fields=[["Case_Count", "SUM"]], case_field=["Onset_Date_UTC"])
CasesByDate = arcpy.AlterField_management(in_table="CasesByDate", field="SUM_Case_Count", new_field_name="NewCases", new_field_alias="New cases", field_type="", field_length=8, field_is_nullable="NULLABLE", clear_field_alias="DO_NOT_CLEAR")[0]
CasesByDate = arcpy.AddField_management(in_table=CasesByDate, field_name="TotalCases", field_type="LONG", field_precision=None, field_scale=None, field_length=None, field_alias="Total cases", field_is_nullable="NULLABLE", field_is_required="NON_REQUIRED", field_domain="")[0]
CasesByDate = arcpy.CalculateField_management(in_table=CasesByDate, field="TotalCases", expression="accumulate(!NewCases!)", expression_type="PYTHON3", code_block=accumulateOverallCB, field_type="TEXT")[0]
CasesByDate = arcpy.DeleteField_management(in_table=CasesByDate, drop_field=["FREQUENCY"])[0]

# Summarize deaths by date
message = "Summarize deaths by date"
print(message)
logfile.write("{0}\t{1}\n".format(datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S"), message))
arcpy.Statistics_analysis(in_table=odhSummary, out_table="DeathsByDate", statistics_fields=[["Death_Count", "SUM"]], case_field=["Date_Of_Death_UTC"])
DeathsByDate = arcpy.AlterField_management(in_table="DeathsByDate", field="SUM_Death_Count", new_field_name="NewDeaths", new_field_alias="New Deaths", field_type="", field_length=8, field_is_nullable="NULLABLE", clear_field_alias="DO_NOT_CLEAR")[0]
DeathsByDate = arcpy.AddField_management(in_table=DeathsByDate, field_name="TotalDeaths", field_type="LONG", field_precision=None, field_scale=None, field_length=None, field_alias="Total deaths", field_is_nullable="NULLABLE", field_is_required="NON_REQUIRED", field_domain="")[0]
DeathsByDate = arcpy.CalculateField_management(in_table=DeathsByDate, field="TotalDeaths", expression="accumulate(!NewDeaths!)", expression_type="PYTHON3", code_block=accumulateOverallCB, field_type="TEXT")[0]
DeathsByDate = arcpy.DeleteField_management(in_table=DeathsByDate, drop_field=["FREQUENCY"])[0]


###########################################################################################################################
# Join county statistics to county boundary polygons and create map layers
# Data source: 
#    U.S. Census Bureau
#    2018 American Community Survey Cartographic Boundary Shapefiles
#    https://www2.census.gov/geo/tiger/GENZ2018/shp/cb_2018_us_county_500k.zip
###########################################################################################################################

message = "Preparing county features"
print(message)
logfile.write("{0}\t{1}\n".format(datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S"), message))
# Select only Ohio counties from Census cartographic boundary polygons using Ohio's state code
if arcpy.Exists("countyFeaturesSelect") and arcpy.env.overwriteOutput:
    arcpy.Delete_management("countyFeaturesSelect")
arcpy.Select_analysis(in_features=countyFeatures, out_feature_class="countyFeaturesSelect", where_clause="STATEFP = '39'")

# Reproject selected features to Web Mercator and save as a new feature class
if arcpy.Exists("OhioCounties") and arcpy.env.overwriteOutput:
    arcpy.Delete_management("OhioCounties")
arcpy.Project_management(in_dataset="countyFeaturesSelect", out_dataset="OhioCounties", out_coor_system="PROJCS['WGS_1984_Web_Mercator_Auxiliary_Sphere',GEOGCS['GCS_WGS_1984',DATUM['D_WGS_1984',SPHEROID['WGS_1984',6378137.0,298.257223563]],PRIMEM['Greenwich',0.0],UNIT['Degree',0.0174532925199433]],PROJECTION['Mercator_Auxiliary_Sphere'],PARAMETER['False_Easting',0.0],PARAMETER['False_Northing',0.0],PARAMETER['Central_Meridian',0.0],PARAMETER['Standard_Parallel_1',0.0],PARAMETER['Auxiliary_Sphere_Type',0.0],UNIT['Meter',1.0]]", transform_method=["WGS_1984_(ITRF00)_To_NAD_1983"], in_coor_system="GEOGCS['GCS_North_American_1983',DATUM['D_North_American_1983',SPHEROID['GRS_1980',6378137.0,298.257222101]],PRIMEM['Greenwich',0.0],UNIT['Degree',0.0174532925199433]]", preserve_shape="NO_PRESERVE_SHAPE", max_deviation="", vertical="NO_VERTICAL")
arcpy.Delete_management("countyFeaturesSelect")

message = "Create total cases feature layer"
print(message)
logfile.write("{0}\t{1}\n".format(datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S"), message))
# Create a feature layer for the county polygons and join data from statistics tables created above
arcpy.MakeFeatureLayer_management(in_features="OhioCounties", out_layer="Total cases", where_clause="", workspace="", field_info="OBJECTID OBJECTID VISIBLE NONE;Shape Shape VISIBLE NONE;STATEFP STATEFP VISIBLE NONE;COUNTYFP COUNTYFP VISIBLE NONE;COUNTYNS COUNTYNS VISIBLE NONE;AFFGEOID AFFGEOID VISIBLE NONE;GEOID GEOID VISIBLE NONE;NAME NAME VISIBLE NONE;LSAD LSAD VISIBLE NONE;ALAND ALAND VISIBLE NONE;AWATER AWATER VISIBLE NONE;Shape_length Shape_length VISIBLE NONE;Shape_area Shape_area VISIBLE NONE;Shape_length Shape_length VISIBLE NONE;Shape_area Shape_area VISIBLE NONE")
arcpy.JoinField_management(in_data="Total cases", in_field="NAME", join_table="TotalCasesByCounty", join_field="County", fields=["TotalCases"])[0]
arcpy.JoinField_management(in_data="Total cases", in_field="NAME", join_table="TotalDeathsByCounty", join_field="County", fields=["TotalDeaths"])[0]
arcpy.JoinField_management(in_data="Total cases", in_field="NAME", join_table="TotalCasesByCountyBySex", join_field="County", fields=["Female", "Male", "Unknown"])[0]
arcpy.JoinField_management(in_data="Total cases", in_field="NAME", join_table="TotalCasesByCountyByAge", join_field="County", fields=["AGE_0_19", "AGE_20_29", "AGE_30_39", "AGE_40_49", "AGE_50_59", "AGE_60_69", "AGE_70_79", "AGE_80_"])[0]
arcpy.JoinField_management(in_data="Total cases", in_field="NAME", join_table="TotalHospitalizationsByCounty", join_field="County", fields=["TotalHospitalizations"])[0]

message = "Applying symbology to total cases layer"
print(message)
logfile.write("{0}\t{1}\n".format(datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S"), message))

# Apply predefined symbology to represent the total case count in each county
arcpy.ApplySymbologyFromLayer_management(in_layer="Total cases", in_symbology_layer=casesLayerFile, symbology_fields=[["VALUE_FIELD", "TotalCases", "TotalCases"]], update_symbology="UPDATE")[0]

# Create a feature layer for the county polygons and apply predefined symbology to 
# represent counties by their boundaries.  Label counties by their names.
message = "Create county boundaries feature layer"
print(message)
logfile.write("{0}\t{1}\n".format(datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S"), message))
arcpy.MakeFeatureLayer_management(in_features="OhioCounties", out_layer="Counties", where_clause="", workspace="", field_info="OBJECTID OBJECTID VISIBLE NONE;Shape Shape VISIBLE NONE;STATEFP STATEFP VISIBLE NONE;COUNTYFP COUNTYFP VISIBLE NONE;COUNTYNS COUNTYNS VISIBLE NONE;AFFGEOID AFFGEOID VISIBLE NONE;GEOID GEOID VISIBLE NONE;NAME NAME VISIBLE NONE;LSAD LSAD VISIBLE NONE;ALAND ALAND VISIBLE NONE;AWATER AWATER VISIBLE NONE;Shape_length Shape_length VISIBLE NONE;Shape_area Shape_area VISIBLE NONE;Shape_length Shape_length VISIBLE NONE;Shape_area Shape_area VISIBLE NONE")
message = "Applying symbology to county boundaries layer"
print(message)
logfile.write("{0}\t{1}\n".format(datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S"), message))
arcpy.ApplySymbologyFromLayer_management(in_layer="Counties", in_symbology_layer=boundariesLayerFile, symbology_fields=[], update_symbology="DEFAULT")[0]

###########################################################################################################################
# Publish layers and tables to ArcGIS Online
###########################################################################################################################

if folderOverride == False:
    message = "Using service definition from draft (no override)"
    print(message)
    logfile.write("{0}\t{1}\n".format(datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S"), message))
    folderType = "FROM_SERVICE_DEFINITION"
    folderName = ""
else:
    message = "Overriding service definition. folderType = {0}, folderName = {1}".format(folderType, folderName)
    print(message)
    logfile.write("{0}\t{1}\n".format(datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S"), message))

message = "Staging service"
print(message)
logfile.write("{0}\t{1}\n".format(datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S"), message))
try:
    serviceDefinition = arcpy.StageService_server(in_service_definition_draft=serviceDraft, out_service_definition=None)
    warnings = arcpy.GetMessages(1)
    print(warnings)
except Exception as stage_exception:
    message = "Sddraft not staged. Analyzer errors encountered - {}".format(str(stage_exception))
    print(message)
    logfile.write("{0}\t{1}\n".format(datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S"), message))
    sys.exit(1)

#input("Press a key to continue")

message = "Uploading service"
print(message)
logfile.write("{0}\t{1}\n".format(datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S"), message))
try:
    arcpy.UploadServiceDefinition_server(in_sd_file=serviceDefinition, in_server="My Hosted Services", in_service_name="", in_cluster="", in_folder_type=folderType, in_folder=folderName, in_startupType="STARTED", in_override="USE_DEFINITION", in_my_contents="NO_SHARE_ONLINE", in_public="PRIVATE", in_organization="NO_SHARE_ORGANIZATION", in_groups=[])
except Exception as upload_exception:
    message = "Service not uploaded - {}".format(str(upload_exception))
    print(message)
    logfile.write("{0}\t{1}\n".format(datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S"), message))
    sys.exit(1)
	
message = "Script completed successfully"
print(message)
logfile.write("{0}\t{1}\n".format(datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S"), message))

logfile.close()

sys.exit(0)